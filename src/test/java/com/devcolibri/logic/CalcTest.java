package com.devcolibri.logic;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.*;

public class CalcTest {
    private Calc calc = new Calc();

    @Test(groups = "StaticTest")
    public void TestSum() throws Exception {
        Assert.assertEquals(5,calc.Sum(2,3),5/1e3);
    }

    @Test(groups = "StaticTest")
    public void TestMult() throws Exception {
        Assert.assertEquals(6.0,calc.Mult(2,3));
    }

    @Test(groups = {"StaticTest","TestDiv"})
    public void TestDiv() throws Exception {
        Assert.assertEquals(2.5,calc.Div(5,2));
    }
// Проверка деления на ноль
    @Test(groups = {"StaticTest","TestDiv"})
    public void TestDiv0() throws Exception {
        Assert.assertEquals("Infinity",String.format("%.3f", calc.Div(5,0)));
        System.out.println("TestingMult: actual:Infinity; exception:"+String.format("%.3f",calc.Div(5,0)));
    }

    @Test(groups = "StaticTest")
    public void TestSup() throws Exception {
        Assert.assertEquals(-1.0,calc.Sup(2,3));
    }
    @DataProvider(parallel = true,name = "TestDataSum")
    public Object[][] TestDataSum(){
        return new Object[][]{
                {3,5,8},
                {2,3,5},
                {10,145,155},
                {5.5,5.5,11},
                {-5.5,5.5,0},
                {-5.5,-5.5,-11},
                {0,-1,-1},
                {0,-0.1,-0.1},
                {0,+1,1},
                {0,0,0},

        };

    }

    @Test(dataProvider = "TestDataSum",groups = "GroupTest")
    public void TestSumData(double a, double b, double actual )throws Exception {
        Assert.assertEquals(actual,calc.Sum(a,b));
        System.out.println("TestingMult: actual:"+String.format("%.3f",actual)+"; exception:"+String.format("%.3f",calc.Sum(a,b)));
    }

    @DataProvider(name = "TestDataDiv")
    public Object[][] TestDataDiv(){
        return new Object[][]{
                {5,2,2.5},
                {10,2,5},
                {0,2,0},
                {0.5,5,0.1},
                {0.5,0.5,1},
                {6,77,0.078},
                {5,0.5,10},
                {1,3,0.333},
                {6,7,0.857},


        };
    }
    @Test(dataProvider ="TestDataDiv", groups = {"GroupTest", "TestDiv"})
    public void  TestDivData(double a, double b, double actual) throws Exception {
        Assert.assertEquals(String.format("%.3f",actual), String.format("%.3f",calc.Div(a,b)));
        System.out.println("TestingDiv: actual:"+String.format("%.3f",actual)+"; exception:"+String.format("%.3f",calc.Div(a,b)));

    }

    @DataProvider (name = "TestDataMult")
    public  Object[][] TestDataMult(){
        return new Object[][]{
                {1,1,1},
                {2,2,4},
                {-2,2,-4},
                {-2,-2,4},
                {0,2,0},
                {0,0,0},
                {2,0,0},
                {2,1,2},
                {10,10,100},
                {15.156,15.156,229.704}
        };
    }

    @Test(dataProvider ="TestDataMult",groups = "GroupTest")
    public  void TestMultData (double a, double b, double actual) throws Exception {
        Assert.assertEquals(String.format("%.3f", actual),String.format("%.3f", calc.Mult(a,b)));
        System.out.println("TestingMult: actual:"+String.format("%.3f",actual)+"; exception:"+String.format("%.3f",calc.Mult(a,b)));
    }

    @DataProvider(name = "TestDataSup")
    public Object[][] TestDataSup(){
        return new Object[][]{
                {2,2,0},
                {2,-2,4},
                {-2,-2,0},
                {-2,2,-4},
                {5,-2.7,7.7},
                {5,2.7,2.3},

        };
    }

    @Test(dataProvider = "TestDataSup", groups = "GroupTest")
    public void TestDataSup (double a, double b,double actual ) throws Exception {
        Assert.assertEquals(actual, calc.Sup(a,b));
        System.out.println("TestingSup: actual:"+String.format("%.3f",actual)+"; exception:"+String.format("%.3f",calc.Sup(a,b)));
    }

}

